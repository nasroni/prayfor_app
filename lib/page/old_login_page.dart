import 'dart:async';
import 'dart:math';

import 'package:http/http.dart' as http;

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

class OldLoginPage extends StatefulWidget {
  OldLoginPage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<OldLoginPage> with WidgetsBindingObserver {
  String payload = "";
  String token = "";
  String prename = "";
  Timer timer;
  bool timerWasActive = true;

  @override
  void initState() {
    requestPayload();
    WidgetsBinding.instance.addObserver(this);
    timer = Timer.periodic(
        Duration(milliseconds: 600), (Timer t) => requestToken());
    super.initState();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if ((state == AppLifecycleState.inactive ||
            state == AppLifecycleState.paused) &&
        timer.isActive) {
      timer?.cancel();
      timerWasActive = true;
    } else if (state == AppLifecycleState.resumed && timerWasActive) {
      requestToken();
      timer = Timer.periodic(
          Duration(milliseconds: 600), (Timer t) => requestToken());
    }
    super.didChangeAppLifecycleState(state);
  }

  void requestPayload() async {
    String genString = "";

    SharedPreferences prefs = await SharedPreferences.getInstance();
    DateTime td = DateTime.now();
    String todayString = td.hour.toString() +
        td.day.toString() +
        td.year.toString() +
        td.month.toString();
    if (prefs.containsKey(todayString)) {
      genString = prefs.getString(todayString);
    } else {
      String chars =
          "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
      var rng = new Random();
      var ln = chars.length;
      for (var i = 0; i < 20; i++) {
        genString += chars[rng.nextInt(ln)];
      }
      prefs.setString(todayString, genString);
    }
    setState(() {
      payload = genString;
    });
  }

  void requestToken() async {
    var url = "https://prayfor.nasroni.one/get/token/$payload";
    if (payload == "") return;
    if (token != "") return;
    var response = await http.get(url);
    print(response.body);
    if (response.body != "undefined" && response.body.length == 20) {
      timer.cancel();
      setState(() {
        token = response.body;
      });
      requestPrename();
    }
  }

  void requestPrename() async {
    var url = "https://prayfor.nasroni.one/get/user/$token";
    var response = await http.get(url);
    setState(() {
      prename = response.body;
    });
  }

  void _openBot() async {
    String url = "https://t.me/prayforapp_bot?start=$payload";
    if (await canLaunch(url))
      await launch(url);
    else
      throw 'Could not launch $url';
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('Payload: $payload'),
            Text('Token: $token'),
            Text('Username: $prename'),
            IconButton(
              icon: Icon(Icons.send),
              onPressed: payload == "" ? null : _openBot,
            ),
            FlatButton(
                child: Text("open homepage"),
                onPressed: () => Navigator.pop(context))
          ],
        ),
      ),
    );
  }
}
