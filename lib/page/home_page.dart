import 'package:PrayFor/store/base_store.dart';
import 'package:PrayFor/store/login_store.dart';
import 'package:PrayFor/store/prayer_store.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:provider/provider.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final baseStore = Provider.of<BaseStore>(context);
    final loginStore = Provider.of<LoginStore>(context);
    final prayerStore = Provider.of<PrayerStore>(context);

    baseStore.setupReactions();
    // final prayerStore = baseStore.prayerStore;

    return Scaffold(
      appBar: AppBar(
        title:
            Observer(builder: (_) => Text("User: ${baseStore.user.username}")),
      ),
      body: Column(
        children: <Widget>[
          Center(
            child: RaisedButton(
              onPressed: () => loginStore.refreshUser(),
              child: Text("refresh"),
            ),
          ),
          Center(
            child: RaisedButton(
              onPressed: () async {
                await loginStore.logout();
                await Navigator.of(context).pushNamed('/login');
                // loginStore.checkUsername();
              },
              child: Text("logout"),
            ),
          ),
          Center(
            child: Observer(builder: (_) {
              return Text(prayerStore.base.user.emoji);
            }),
          ),
          Center(
            child: RaisedButton(
              onPressed: () => null,
              child: Text("get"),
            ),
          ),
          Center(
            child: RaisedButton(
              onPressed: () => prayerStore.addPrayer(),
              child: Text('Add'),
            ),
          ),
          Center(
            child: RaisedButton(
              onPressed: () => prayerStore.updatePrayer(),
              child: Observer(builder: (_) {
                return Text(prayerStore.thisPrayer?.title ?? "update");
              }),
            ),
          ),
          Center(
            child: RaisedButton(
              onPressed: () => prayerStore.removeAllPrayers(),
              child: Text("removeall"),
            ),
          ),
          Observer(
            builder: (_) {
              if (baseStore.user.loggedIn)
                return CachedNetworkImage(
                  imageUrl:
                      "https://prayfor.nasroni.one/profile_pics/${baseStore.user.id}.jpg",
                  placeholder: (context, url) =>
                      new CircularProgressIndicator(),
                  errorWidget: (context, url, error) => new Icon(Icons.error),
                  width: 220,
                );
              else
                return Container();
            },
          ),
        ],
      ),
    );
  }
}
