import 'package:PrayFor/store/login_store.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mobx/mobx.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class LoginPage extends StatelessWidget {
  LoginPage({Key key}) : super(key: key);

  final Color backgroundColor = const Color.fromRGBO(241, 255, 235, 1);
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  Widget iconImage() => Container(
        alignment: Alignment.center,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(20),
          child: const Image(
            image: AssetImage('assets/logo/logo.png'),
            height: 150,
          ),
        ),
      );
  Widget appTitle() => Text(
        "PrayFor",
        style: GoogleFonts.ptSerif(
          fontSize: 35,
          letterSpacing: 1,
        ),
      );
  Widget telegramButton(payload) => Container(
        width: 150,
        child: RaisedButton(
          color: Color.fromRGBO(37, 163, 225, 1),
          disabledColor: Colors.grey[600],
          textColor: Colors.white,
          disabledTextColor: Colors.white,
          highlightColor: Colors.blue[300],
          onPressed: () => _openBot(payload),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: <Widget>[
                FaIcon(FontAwesomeIcons.telegramPlane),
                Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: Text(
                    "LOGIN",
                    style: TextStyle(fontSize: 22, letterSpacing: 1),
                  ),
                )
              ],
            ),
          ),
        ),
      );

  void _openBot(payload) async {
    String url = "https://t.me/prayforapp_bot?start=$payload";
    if (await canLaunch(url))
      await launch(url);
    else
      throw 'Could not launch $url';
  }

  @override
  Widget build(BuildContext context) {
    final loginStore = Provider.of<LoginStore>(context, listen: false);
    reaction((_) => loginStore.state, (r) async {
      if (r == StoreState.loggedIn) {
        _scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text("Hi ${loginStore.base.user.prename} 👋"),
        ));
        await Future.delayed(Duration(seconds: 2));
        Navigator.of(context).pop();
      }
    });

    return WillPopScope(
      onWillPop: () => null,
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: backgroundColor,
        body: OverflowBox(
          maxHeight: 2000,
          alignment: Alignment.topCenter,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Observer(
                builder: (_) => AnimatedPadding(
                  duration: const Duration(milliseconds: 1500),
                  padding: EdgeInsets.only(
                      top: loginStore.state == StoreState.payload ? 200 : 300),
                  child: iconImage(),
                ),
              ),
              appTitle(),
              Observer(
                builder: (_) => AnimatedPadding(
                    duration: const Duration(milliseconds: 1500),
                    padding: EdgeInsets.only(
                        top:
                            loginStore.state == StoreState.payload ? 150 : 500),
                    child: telegramButton(loginStore.payload)),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
