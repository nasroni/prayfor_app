import 'package:PrayFor/page/home_page.dart';
import 'package:PrayFor/page/login_page.dart';
import 'package:PrayFor/service/login_api.dart';
import 'package:PrayFor/store/base_store.dart';
import 'package:PrayFor/store/login_store.dart';
import 'package:PrayFor/store/prayer_store.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        // Create a basestore
        Provider<BaseStore>(
          create: (_) => BaseStore(LoginApi()),
        ),
        // And add the substores for easier access
        ProxyProvider<BaseStore, LoginStore>(
          update: (_, baseStore, __) => baseStore.loginStore,
        ),
        ProxyProvider<BaseStore, PrayerStore>(
          update: (_, baseStore, __) => baseStore.prayerStore,
        )
      ],
      child: AppWithLifecycleState(),
    );
  }
}

class AppWithLifecycleState extends StatefulWidget {
  const AppWithLifecycleState({
    Key key,
  }) : super(key: key);

  @override
  _AppWithLifecycleStateState createState() => _AppWithLifecycleStateState();
}

// BaseStore should know if app is in foreground
class _AppWithLifecycleStateState extends State<AppWithLifecycleState>
    with WidgetsBindingObserver {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MaterialApp(
      title: 'PrayFor',
      theme: ThemeData(
        primarySwatch: Colors.teal,
      ),
      initialRoute: '/login',
      routes: {
        '/': (context) => HomePage(),
        '/login': (context) => LoginPage(),
      },
    );
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // The effective code
    Provider.of<BaseStore>(context, listen: false).uiState = state;
  }

  @override
  initState() {
    WidgetsBinding.instance.addObserver(this);
    super.initState();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }
}
