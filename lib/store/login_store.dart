import 'dart:async';

import 'package:PrayFor/model/user.dart';
import 'package:PrayFor/service/login_api.dart';
import 'package:PrayFor/store/base_store.dart';
import 'package:mobx/mobx.dart';

part 'login_store.g.dart';

class LoginStore = _LoginStore with _$LoginStore;

enum StoreState { initial, payload, noInternet, loggedIn }

abstract class _LoginStore with Store {
  final LoginApi _loginApi;
  final BaseStore base;

  _LoginStore(this._loginApi, {this.base}) {
    checkUsername();
  }

  Timer timer;
  void checkUsername([Timer timer]) async {
    if (StoreState.payload != this.state && StoreState.initial != this.state)
      return;
    base.user = await _loginApi.getUser();
    if (base.user.loggedIn && timer != null) {
      timer.cancel();
    }
    if (!base.user.loggedIn && timer == null) {
      getPayload();
      timer = Timer.periodic(const Duration(milliseconds: 600), checkUsername);
    }
  }

  @observable
  String payload;

  @computed
  StoreState get state {
    if (base.user.loggedIn) {
      timer?.cancel();
      return StoreState.loggedIn;
    }
    if (payload != null) return StoreState.payload;
    return StoreState.initial;
  }

  @action
  refreshUser() async {
    UserData newUser = await _loginApi.refreshUser();
    if (newUser != null) base.user = newUser;
  }

  @action
  getPayload() async {
    payload = await _loginApi.getPayload();
  }

  @observable
  @action
  logout() async {
    await _loginApi.logout();
    await base.logout();
    payload = null;
    checkUsername();
  }
}
