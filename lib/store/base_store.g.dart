// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'base_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$BaseStore on _BaseStore, Store {
  final _$userAtom = Atom(name: '_BaseStore.user');

  @override
  UserData get user {
    _$userAtom.reportRead();
    return super.user;
  }

  @override
  set user(UserData value) {
    _$userAtom.reportWrite(value, super.user, () {
      super.user = value;
    });
  }

  final _$uiStateAtom = Atom(name: '_BaseStore.uiState');

  @override
  AppLifecycleState get uiState {
    _$uiStateAtom.reportRead();
    return super.uiState;
  }

  @override
  set uiState(AppLifecycleState value) {
    _$uiStateAtom.reportWrite(value, super.uiState, () {
      super.uiState = value;
    });
  }

  final _$logoutAsyncAction = AsyncAction('_BaseStore.logout');

  @override
  Future logout() {
    return _$logoutAsyncAction.run(() => super.logout());
  }

  @override
  String toString() {
    return '''
user: ${user},
uiState: ${uiState}
    ''';
  }
}
