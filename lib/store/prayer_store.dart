import 'package:PrayFor/model/prayer.dart';
import 'package:PrayFor/service/prayer_api.dart';
import 'package:PrayFor/store/base_store.dart';
import 'package:mobx/mobx.dart';
import 'package:uuid/uuid.dart';

part 'prayer_store.g.dart';

class PrayerStore = _PrayerStore with _$PrayerStore;

abstract class _PrayerStore with Store {
  final BaseStore base;
  _PrayerStore({this.base});

  PrayerApi prayerApi = PrayerApi();

  @observable
  Prayer thisPrayer;

  @action
  addPrayer() async {
    var prayer = Prayer(
        id: Uuid().v1(),
        creationTime: DateTime.now().toIso8601String(),
        creatorId: base.user.id,
        groupId: "e4a61665-3f5e-414e-9cb5-026bb746ee07",
        title: "Anliegen 2",
        type: PrayerType.request,
        events: [
          PrayerEvent(
              content: 'salüü',
              id: Uuid().v1(),
              writeTime: DateTime.now().toIso8601String(),
              writerId: base.user.id)
        ]);
    await prayerApi.insertPrayer(prayer);
    await prayerApi.insertNewestEvent(prayer);
    getPrayer();
  }

  @action
  getPrayer() async {
    var prayers = await prayerApi.prayers();
    if ((prayers?.length ?? 0) > 0)
      thisPrayer = prayers[0];
    else
      thisPrayer = null;
  }

  @action
  updatePrayer() async {
    var prayer = Prayer(
        id: thisPrayer.id,
        creationTime: DateTime.now().toIso8601String(),
        creatorId: base.user.id,
        groupId: "e4a61665-3f5e-414e-9cb5-026bb746ee07",
        title: "Anliegen 3",
        type: PrayerType.request,
        events: [
          PrayerEvent(
            content: 'salüü',
            id: Uuid().v1(),
            writeTime: DateTime.now().toIso8601String(),
            writerId: base.user.id,
          ),
          PrayerEvent(
            content: 'oder so',
            id: Uuid().v1(),
            writeTime: DateTime.now().toIso8601String(),
            writerId: base.user.id,
          ),
        ]);
    await prayerApi.updatePrayer(prayer);
    getPrayer();
  }

  @action
  removeAllPrayers() async {
    var allPrayers = await prayerApi.prayers();
    for (Prayer prayer in allPrayers) {
      await prayerApi.deletePrayer(prayer.id);
    }
    getPrayer();
  }

  @action
  logout() async {
    await removeAllPrayers();
  }
}
