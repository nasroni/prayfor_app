// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'prayer_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$PrayerStore on _PrayerStore, Store {
  final _$thisPrayerAtom = Atom(name: '_PrayerStore.thisPrayer');

  @override
  Prayer get thisPrayer {
    _$thisPrayerAtom.reportRead();
    return super.thisPrayer;
  }

  @override
  set thisPrayer(Prayer value) {
    _$thisPrayerAtom.reportWrite(value, super.thisPrayer, () {
      super.thisPrayer = value;
    });
  }

  final _$addPrayerAsyncAction = AsyncAction('_PrayerStore.addPrayer');

  @override
  Future addPrayer() {
    return _$addPrayerAsyncAction.run(() => super.addPrayer());
  }

  final _$getPrayerAsyncAction = AsyncAction('_PrayerStore.getPrayer');

  @override
  Future getPrayer() {
    return _$getPrayerAsyncAction.run(() => super.getPrayer());
  }

  final _$updatePrayerAsyncAction = AsyncAction('_PrayerStore.updatePrayer');

  @override
  Future updatePrayer() {
    return _$updatePrayerAsyncAction.run(() => super.updatePrayer());
  }

  final _$removeAllPrayersAsyncAction =
      AsyncAction('_PrayerStore.removeAllPrayers');

  @override
  Future removeAllPrayers() {
    return _$removeAllPrayersAsyncAction.run(() => super.removeAllPrayers());
  }

  final _$logoutAsyncAction = AsyncAction('_PrayerStore.logout');

  @override
  Future logout() {
    return _$logoutAsyncAction.run(() => super.logout());
  }

  @override
  String toString() {
    return '''
thisPrayer: ${thisPrayer}
    ''';
  }
}
