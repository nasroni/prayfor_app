// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$LoginStore on _LoginStore, Store {
  Computed<StoreState> _$stateComputed;

  @override
  StoreState get state => (_$stateComputed ??=
          Computed<StoreState>(() => super.state, name: '_LoginStore.state'))
      .value;

  final _$payloadAtom = Atom(name: '_LoginStore.payload');

  @override
  String get payload {
    _$payloadAtom.reportRead();
    return super.payload;
  }

  @override
  set payload(String value) {
    _$payloadAtom.reportWrite(value, super.payload, () {
      super.payload = value;
    });
  }

  final _$refreshUserAsyncAction = AsyncAction('_LoginStore.refreshUser');

  @override
  Future refreshUser() {
    return _$refreshUserAsyncAction.run(() => super.refreshUser());
  }

  final _$getPayloadAsyncAction = AsyncAction('_LoginStore.getPayload');

  @override
  Future getPayload() {
    return _$getPayloadAsyncAction.run(() => super.getPayload());
  }

  final _$logoutAsyncAction = AsyncAction('_LoginStore.logout');

  @override
  ObservableFuture logout() {
    return ObservableFuture(_$logoutAsyncAction.run(() => super.logout()));
  }

  @override
  String toString() {
    return '''
payload: ${payload},
state: ${state}
    ''';
  }
}
