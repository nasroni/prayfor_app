import 'dart:async';
import 'dart:ui';

import 'package:PrayFor/model/user.dart';
import 'package:PrayFor/service/login_api.dart';
import 'package:PrayFor/store/login_store.dart';
import 'package:PrayFor/store/prayer_store.dart';
import 'package:mobx/mobx.dart';

part 'base_store.g.dart';

class BaseStore = _BaseStore with _$BaseStore;

abstract class _BaseStore with Store {
  // Injected APIs
  final LoginApi _loginApi;

  // Sub Stores
  LoginStore loginStore;
  PrayerStore prayerStore;

  // Constructor
  _BaseStore(this._loginApi) {
    prayerStore = PrayerStore(base: this);
    loginStore = LoginStore(_loginApi, base: this);
  }

  // Very important oberservables
  @observable
  UserData user = UserData();

  @observable
  AppLifecycleState uiState = AppLifecycleState.resumed;

  // Sync timer
  void setupReactions() {
    when((_) => user.loggedIn, () {
      if (timer == null)
        timer = Timer.periodic(const Duration(seconds: 5), (timer) {
          // Check if App in Foreground
          if (uiState == AppLifecycleState.resumed) {
            loginStore.refreshUser();
          }
        });
    });
  }

  Timer timer;

  @action
  logout() async {
    user = UserData();
    await prayerStore.logout();
  }
}
