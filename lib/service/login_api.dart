import 'dart:convert';
import 'dart:math';
import 'package:http/http.dart' as http;

import 'package:PrayFor/model/user.dart';
import 'package:PrayFor/service/settings_api.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginApi {
  String _payload;
  String _secureToken;
  SettingsAPI _settings = SettingsAPI();

  Future<String> getPayload() async {
    _payload = "";

    // check if payload exists from the current hour
    SharedPreferences prefs = await SharedPreferences.getInstance();
    DateTime td = DateTime.now();
    String todayString = td.hour.toString() +
        td.day.toString() +
        td.year.toString() +
        td.month.toString();
    if (prefs.containsKey(todayString)) {
      _payload = prefs.getString(todayString);
    } else {
      String chars =
          "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
      var rng = new Random();
      var ln = chars.length;
      for (var i = 0; i < 20; i++) {
        _payload += chars[rng.nextInt(ln)];
      }
      prefs.setString(todayString, _payload);
    }
    return _payload;
  }

  Future<String> _getToken() async {
    if (_secureToken != null) return _secureToken;
    _secureToken = await _settings.token;
    if (_secureToken != null) return _secureToken;
    _secureToken = await _requestToken();
    _settings.token = _secureToken;
    return _secureToken;
  }

  Future<String> _requestToken() async {
    String url = "https://prayfor.nasroni.one/get/token/$_payload";
    var response = await http.get(url);
    if (response.body != "undefined" && response.body.length == 20) {
      return response.body;
    }
    return null;
  }

  Future<UserData> getUser() async {
    UserData user = await _settings.user;
    if (user.loggedIn) return user;
    String token = await _getToken();
    if (token == null) return UserData(id: 0, prename: "");
    user = await _requestUser();
    return user;
  }

  Future<UserData> refreshUser() async {
    _secureToken = await _getToken();
    if (_secureToken == null) return null;
    return await _requestUser();
  }

  Future<UserData> _requestUser() async {
    var url = "https://prayfor.nasroni.one/get/user/$_secureToken";
    var json = jsonDecode((await http.get(url)).body);
    if (json == null) return null;
    var user = UserData(
      id: int.tryParse(json['id']),
      prename: json['prename'],
      lastname: json['lastname'],
      emoji: json['emoji'],
    );
    _settings.setUser(user);
    return user;
  }

  logout() async {
    await _settings.reset();
    _payload = null;
    _secureToken = null;
  }
}
