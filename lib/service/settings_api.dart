import 'package:PrayFor/model/user.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingsAPI {
  static const String tokenKey = "tokenkey";
  static const String userIdKey = "useridkey";
  static const String userPreNameKey = "userprenkey";
  static const String userLastNameKey = "userlastnkey";
  static const String userEmojiKey = "usermojikey";

  final _storage = new FlutterSecureStorage();
  final _prefFuture = SharedPreferences.getInstance();

  Future<String> get token async {
    String value = await _storage.read(key: tokenKey);
    return value;
  }

  set token(value) {
    if (value != null) _storage.write(key: tokenKey, value: value);
  }

  Future<UserData> get user async {
    if (tokenKey == null) return UserData();
    var preferences = await _prefFuture;
    int id = _getIntEntry(preferences, userIdKey) ?? 0;
    String prename = _getStringEntry(preferences, userPreNameKey) ?? "";
    String lastname = _getStringEntry(preferences, userLastNameKey) ?? "";
    String emoji = _getStringEntry(preferences, userEmojiKey) ?? "";
    return UserData(id: id, prename: prename, lastname: lastname, emoji: emoji);
  }

  setUser(UserData value) async {
    var preferences = await _prefFuture;
    preferences.setInt(userIdKey, value.id);
    preferences.setString(userPreNameKey, value.prename);
    preferences.setString(userLastNameKey, value.lastname);
    preferences.setString(userEmojiKey, value.emoji);
  }

  _getIntEntry(SharedPreferences p, String key) {
    if (p.containsKey(key))
      return p.getInt(key);
    else
      return null;
  }

  _getStringEntry(SharedPreferences p, String key) {
    if (p.containsKey(key))
      return p.getString(key);
    else
      return null;
  }

  reset() async {
    var preferences = await _prefFuture;
    await preferences.clear();
    await _storage.deleteAll();
  }
}
