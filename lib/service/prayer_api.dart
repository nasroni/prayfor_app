import 'dart:async';

import 'package:PrayFor/model/prayer.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite_migration/sqflite_migration.dart';

class PrayerApi {
  final initialScript = [
    '''CREATE TABLE IF NOT EXISTS deletes(
            id STRING PRIMARY KEY,
            tablename STRING,
            deleteTime TEXT
        )''',
    '''CREATE TABLE IF NOT EXISTS events(
          id STRING PRIMARY KEY,
          writeTime TEXT,
          writerId INTEGER,
          prayerId STRING,
          content TEXT
        )''',
    '''CREATE TABLE IF NOT EXISTS prayers(
              id STRING PRIMARY KEY, 
              title TEXT,
              type INTEGER,
              groupId STRING, 
              creatorId INTEGER, 
              creationTime TEXT,
              targetTime TEXT,
              answeredTime TEXT,
              isImportant INTEGER,
        )''',
  ];

  final migrations = [];

  Future<Database> database() async {
    var databasesPath = await getDatabasesPath();
    String path = join(databasesPath, 'prayers.db');

    await deleteDatabase(path);

    final config = MigrationConfig(
        initializationScript: initialScript, migrationScripts: migrations);

    return await openDatabaseWithMigration(path, config);
  }

  Future<void> insertPrayer(Prayer prayer) async {
    final Database db = await database();

    await db.insert(
      'prayers',
      prayer.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<void> insertNewestEvent(Prayer prayer) async {
    final Database db = await database();

    var event = prayer.eventToMap(prayer.events.length - 1);

    await db.insert(
      'events',
      event,
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<List<PrayerEvent>> events(String prayerId) async {
    final Database db = await database();

    final List<Map<String, dynamic>> maps = await db.query(
      'events', // TODO: fix this
      // where: 'prayerId = ?',
      // whereArgs: [prayerId],
    );

    return List.generate(
      maps.length,
      (index) => PrayerEvent(
        id: maps[index]['id'],
        writeTime: maps[index]['prayerId'],
        writerId: maps[index]['writerId'],
        content: maps[index]['content'],
      ),
    );
  }

  Future<List<Prayer>> prayers() async {
    final Database db = await database();

    final List<Map<String, dynamic>> maps = await db.query('prayers');

    List<Prayer> list = List<Prayer>();
    for (var element in maps) {
      List<PrayerEvent> prayerEvents = await events(element['id']);
      PrayerType prayerType = PrayerType.values[element['type']];

      list.add(Prayer(
        id: element['id'],
        title: element['title'],
        type: prayerType,
        events: prayerEvents,
        groupId: element['groupId'],
        creatorId: element['creatorId'],
        creationTime: element['creationTime'],
        targetTime: element['targetTime'],
        answeredTime: element['answeredTime'],
        isImportant: element['isImportant'],
      ));
    }

    return list;
  }

  Future<void> updatePrayer(Prayer prayer) async {
    final Database db = await database();

    await db.update(
      'prayers',
      prayer.toMap(),
      where: "id = ?",
      whereArgs: [prayer.id],
    );
  }

  Future<void> deletePrayer(String id) async {
    final Database db = await database();

    await db.delete(
      'prayers',
      where: "id = ?",
      whereArgs: [id],
    );

    // Add deleteTime
    await db.insert(
      'deletes',
      {'tablename': 'prayers', 'id': id},
    );
  }

  Future<void> deleteEvent(String id) async {
    final Database db = await database();

    await db.delete(
      'events',
      where: "id = ?",
      whereArgs: [id],
    );

    await db.insert(
      'deletes',
      {'tablename': 'events', 'id': id},
    );
  }
}
