class UserData {
  UserData(
      {this.id = 0,
      this.prename = "",
      this.emoji = "",
      this.lastname = "",
      this.imgUrl = ""});

  // Base fields
  int id;
  String prename;
  String lastname;
  String emoji;
  String imgUrl;

  // Simple getters
  bool get loggedIn => id != 0;
  String get username => "$prename $emoji";
}
