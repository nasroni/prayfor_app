enum PrayerType { request, thank, praise }

class Prayer {
  // BEGIN: userid, THEN: 6 random digits, END: 7 counted digits
  final String id;
  final String title;
  final PrayerType type;
  final List<PrayerEvent> events;

  final String groupId;

  final int creatorId;
  final String creationTime;

  final String targetTime;
  final String answeredTime;
  final bool isImportant;

  Prayer({
    this.id,
    this.title,
    this.type,
    this.events,
    this.groupId,
    this.creatorId,
    this.creationTime,
    this.targetTime,
    this.answeredTime,
    this.isImportant,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'title': title,
      'type': type.index,
      'groupId': groupId,
      'creatorId': creatorId,
      'creationTime': creationTime,
      'targetTime': targetTime,
      'answeredTime': answeredTime,
      'important': isImportant
    };
  }

  Map<String, dynamic> eventToMap(int listId) {
    var event = events[listId];
    return {
      'id': event.id,
      'writeTime': event.writeTime,
      'writerId': event.writerId,
      'prayerId': id,
      'content': event.content
    };
  }
}

class PrayerEvent {
  final String id;
  final String writeTime;
  final int writerId;
  final String content;

  PrayerEvent({this.id, this.writerId, this.content, this.writeTime});
}
